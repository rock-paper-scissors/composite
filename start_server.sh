#!/bin/bash

cd ./API/

if [ ! -d node_modules/ ]; then
  npm i
fi

if [ ! -f .env ]; then

cat > .env <<- _EOF_
HOST=127.0.0.1
PORT=3000
_EOF_

fi

npm test
npm start
