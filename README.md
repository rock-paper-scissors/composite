# Rock, Paper, Scissors

### Make sure that you have the following dependencies installed on your computer:

```
node.js
npm
```

### Run the app

1. Clone the repo

```
$ git clone --recurse-submodules https://gitlab.com/rock-paper-scissors/composite.git
$ cd ./composite/
```

2. Open a terminal and run:

```
$ ./start_server.sh
```

3. Open another terminal and run:

```
$ ./play.sh
```

